package me.ddevil.mirai

import me.ddevil.mirai.command.*
import me.ddevil.mirai.music.MusicQueue
import me.ddevil.mirai.util.getOrPut
import me.ddevil.mirai.util.getTextChannel
import net.dv8tion.jda.core.AccountType
import net.dv8tion.jda.core.JDA
import net.dv8tion.jda.core.JDABuilder
import net.dv8tion.jda.core.Permission
import net.dv8tion.jda.core.entities.Game
import net.dv8tion.jda.core.entities.Guild
import net.dv8tion.jda.core.entities.MessageChannel
import net.dv8tion.jda.core.entities.User
import net.dv8tion.jda.core.events.message.MessageReceivedEvent
import net.dv8tion.jda.core.hooks.ListenerAdapter
import java.lang.Exception

class Mirai : ListenerAdapter() {
    companion object {
        const val MIRAI_HEADER = "Mirai, "
    }

    val jda: JDA = JDABuilder(AccountType.BOT)
            .setToken("Mjk0OTQ1MzY1MjQzODU0ODQ4.C7chFg.p8tUbfGLI6cnNG3_iRrTn70fQ14")
            .addListener(this)
            .buildBlocking()

    private val guildConfigMap: MutableMap<Guild, GuildConfig> = HashMap()
    var commands: MutableSet<Command> = mutableSetOf()
    var commandChar: Char = '-'
    val kidPreventer = KidPreventer()
    fun getGuildConfig(guild: Guild) = guildConfigMap.getOrPut(guild) {
        GuildConfig(this, guild)
    }

    fun start() {
        val loadedCommands = loadCommands()
        commands.addAll(loadedCommands)
        jda.guilds.forEach {
            val config = getGuildConfig(it)
            sendMessage("大丈夫! システム　OK.", config)
            it.audioManager.sendingHandler = config.musicQueue.wrapper
            debug("${loadedCommands.size} comandos carregados!", config)
        }

        jda.presence.game = Game.of("o loiro num buraco")
    }

    fun getMusicQueue(guild: Guild): MusicQueue {
        return getGuildConfig(guild).musicQueue
    }

    private fun loadCommands() = setOf(
            PingCommand(),
            HelloCommand(),
            CommandPrefixCommand(),
            HelpCommand(),
            MuteCommand(),
            GoogleCommand(),
            EchoCommand(),
            BanCommand(),
            JoinCommand(),
            QuitCommand(),
            GameCommand(),
            AddQueueCommand(),
            PlayCommand(),
            StopCommand(),
            ListQueueCommand(),
            ClearCommand(),
            SkipCommand(),
            VolumeCommand(),
            SetDebugCommand(),
            SetMusicCommand(),
            AddKidWordCommand(),
            RemoveKidWordCommand(),
            ListKidWorldCommand(),
            CommandPermissionCommand(),
            UserPermissionCommand()
    )

    fun getCommand(string: String) = commands.firstOrNull { it.commandName.any { alias -> alias.equals(string, true) } }

    override fun onMessageReceived(event: MessageReceivedEvent) {
        try {

            var content = event.message.content
            if (!content.startsWith(MIRAI_HEADER) && !content.startsWith(commandChar)) {
                //Not for mirai
                return
            }
            if (content.startsWith(MIRAI_HEADER)) {
                content = content.removePrefix(MIRAI_HEADER)
            }
            if (content.startsWith(commandChar)) {
                content = content.removePrefix(commandChar.toString())
            }
            val author = event.author
            val params = content.split(' ')
            if (!params.isNotEmpty() || content.isNullOrEmpty()) {
                sendMessage("${author.asMention}, véi, não é assim q funciona :c", event.channel)
                return
            }
            if (kidPreventer.isKid(content, this, event)) {
                return
            }
            val commandName = params[0]
            val prms: List<String>
            prms = if (params.isNotEmpty()) {
                params.subList(1, params.size)
            } else {
                emptyList()
            }
            val cmd = getCommand(commandName)
            if (cmd == null) {
                sendMessage("Não conheço esse comando :c", event.channel!!)
                return
            }
            val g = event.guild
            if (!g.getMember(event.author).hasPermission(Permission.ADMINISTRATOR) && !getGuildConfig(g).permissionManager.canUse(author, cmd)) {
                sendMessage("Nope.", event.channel)
                return
            }
            val cmdArgs = CommandArgs(this, event.member, commandName, prms)
            cmd.execute(this, cmdArgs, event)
        } catch (e: Exception) {
            sendMessage("Deu ruim! Olha os logs! [${e.message}]", event.channel)
            e.printStackTrace()
        }
    }

    fun sendMessage(s: String, config: GuildConfig) = sendMessage(s, config.defaultChannel)

    fun sendMessage(s: String, channel: MessageChannel) = channel.sendMessage(s).queue()

    fun debug(s: String, config: GuildConfig) = debug(s, config.debugChannel)

    fun debug(s: String, channel: MessageChannel) = channel.sendMessage(s).queue()

    fun sendMessage(s: String, guild: Guild, author: User) {
        val channel = guild.getTextChannel(guild.getMember(author))
        if (channel == null) {
            sendMessage("Canal de usuário ${author.name} desconhecido!", guild)
            return
        }
        sendMessage(s, channel)
    }

    private fun sendMessage(s: String, guild: Guild) {
        sendMessage(s, getGuildConfig(guild))
    }
}

