package me.ddevil.mirai

import net.dv8tion.jda.core.events.message.MessageReceivedEvent

class KidPreventer {
    private val _kidWords = arrayListOf(
            "gay",
            "chupa meu pau",
            "vai tomar no ",
            "vai se fuder",
            "filho da puta",
            "filha da puta",
            "'-'"
    )
    val kidWords: List<String>
        get() = ArrayList(_kidWords)

    fun addKidWord(word: String) {
        _kidWords += word
    }

    fun isKidWord(word: String) = _kidWords.contains(word)

    fun removeKidWord(word: String) {
        _kidWords -= word
    }

    fun isKid(content: String, mirai: Mirai, event: MessageReceivedEvent): Boolean {
        val kid = kidWords.any { content.contains(it) }
        if (kid) {
            mirai.sendMessage("Kid detectado, medidas anti-kid sendo tomadas", event.channel)
            event.guild.controller.kick(event.member).queue()
        }
        return kid
    }

}

