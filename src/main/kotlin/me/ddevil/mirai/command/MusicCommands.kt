package me.ddevil.mirai.command

import com.sedmelluq.discord.lavaplayer.player.AudioLoadResultHandler
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist
import com.sedmelluq.discord.lavaplayer.track.AudioTrack
import me.ddevil.mirai.Mirai
import me.ddevil.mirai.util.getVoiceChannel
import me.ddevil.mirai.util.toText
import net.dv8tion.jda.core.events.message.MessageReceivedEvent

class JoinCommand : Command(1,"Faz mirai entrar no canal especificado", "join", "j") {
    override fun execute(mirai: Mirai, args: CommandArgs, event: MessageReceivedEvent) {
        val guild = event.guild
        val self = guild.selfMember
        val ch = guild.getVoiceChannel(args.sender)
        if (ch == null) {
            mirai.sendMessage("Você não está conectado em nenhum canal de voz!", guild, event.author)
            return
        }
        if (self.voiceState.inVoiceChannel()) {
            guild.controller.moveVoiceMember(self, ch)
        } else {
            guild.audioManager.openAudioConnection(ch)
        }
        mirai.sendMessage("Cheguei nessa caralha", event.channel)
    }
}

class QuitCommand : Command(1,"Faz mirai desconectar do canal de voz (se ela estiver em algum)", "quit", "q") {
    override fun execute(mirai: Mirai, args: CommandArgs, event: MessageReceivedEvent) {
        val guild = event.guild
        val self = guild.selfMember
        if (self.voiceState.inVoiceChannel()) {
            guild.audioManager.closeAudioConnection()
        }
    }

}

class VolumeCommand : Command(1,"Altera o voluma da Mirai", "v", "volume") {
    override fun execute(mirai: Mirai, args: CommandArgs, event: MessageReceivedEvent) {
        args.getIntOrMessage(0, "Você tem q fornecer um volume jegue", "Volume invalid", event.channel) {
            val g = event.guild
            val player = mirai.getMusicQueue(g).player
            val old = player.volume
            player.volume = it
            mirai.sendMessage("Volume alterado para $it (era $old)", mirai.getGuildConfig(g).musicChannel)

        }
    }
}

class SkipCommand : Command(1,"Pula a musica atual", "skip", "s") {
    override fun execute(mirai: Mirai, args: CommandArgs, event: MessageReceivedEvent) {
        val mq = mirai.getMusicQueue(event.guild)
        val ch = event.channel
        if (mq.hasNext()) {
            val track = mq.currentTrack
            if (track != null) {
                mirai.sendMessage("Pulando musica **${track.info.toText}**", ch)
                mq.playNext()

            } else {
                mirai.sendMessage("Não há nenhuma musica tocando no momento!", ch)
            }
        } else {
            mirai.sendMessage("Não há novas musicas para serem tocadas", ch)
        }
    }
}

class ClearCommand : Command(1,"Limpa playlist", "clearPlaylist", "cq") {
    override fun execute(mirai: Mirai, args: CommandArgs, event: MessageReceivedEvent) {
        mirai.getMusicQueue(event.guild).clear()
        mirai.sendMessage("Playlist limpa!", event.channel)
    }
}

class StopCommand : Command(1,"Para de tocar a musica atual", "stop") {
    override fun execute(mirai: Mirai, args: CommandArgs, event: MessageReceivedEvent) {
        mirai.getMusicQueue(event.guild).stop()
    }
}

class PlayCommand : Command(1,"Toca as musicas", "play", "p") {
    override fun execute(mirai: Mirai, args: CommandArgs, event: MessageReceivedEvent) {
        val ch = event.channel
        val mq = mirai.getMusicQueue(event.guild)
        val ct = mq.currentTrack
        if (ct == null) {
            mirai.sendMessage("Não tem nenhuma musica atual tocando!", ch)
            return
        }
        mq.play()
    }
}


class ListQueueCommand : Command(0,"Lista todas as musicas q estão na fila", "listqueue", "lq") {
    override fun execute(mirai: Mirai, args: CommandArgs, event: MessageReceivedEvent) {
        var msg: String
        val mq = mirai.getMusicQueue(event.guild)
        val ct = mq.currentTrack
        if (ct != null) {
            msg = "Tocando atualmente: **${ct.info.toText}**"
        } else {
            msg = "Nenhuma musica tocando atualmente"
        }
        msg += System.lineSeparator()
        if (mq.queue.isEmpty()) {
            msg += "Não há nenhuma musica na fila!"
        } else {
            for (track in mq.queue) {
                val index = mq.queue.indexOf(track)
                if (index > 5) {
                    break
                }
                val info = track.info
                msg += "#$index: ${info.toText}${System.lineSeparator()}"

            }
        }
        mirai.sendMessage(msg, event.channel)
    }
}

class AddQueueCommand : Command(1,"Adiciona uma musica na fila de musicas", "add", "a", "addnext", "an") {
    override fun execute(mirai: Mirai, args: CommandArgs, event: MessageReceivedEvent) {
        val ch = event.channel
        args.getStringOrMessage(0, "Você deve informar uma musica!", event.channel) {
            url ->
            val mq = mirai.getMusicQueue(event.guild)
            mq.audioManager.loadItemOrdered(mq, url, object : AudioLoadResultHandler {

                override fun loadFailed(exception: FriendlyException) {
                    mirai.sendMessage("Houve um erro ao carregar a musica! (${exception.message})", ch)
                }


                override fun trackLoaded(track: AudioTrack) {
                    mq.addToQueue(track)
                    mirai.sendMessage("Musica ${track.info.title} carregada!", ch)
                }

                override fun noMatches() {
                    mirai.sendMessage("Não houve nenhum resultado.", ch)
                }

                override fun playlistLoaded(playlist: AudioPlaylist) {
                    mirai.sendMessage("Playlist carregada. (${playlist.tracks.size} musicas)", ch)
                    mq.addToQueue(playlist)
                }

            })
        }
    }

}