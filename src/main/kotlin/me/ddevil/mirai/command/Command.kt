package me.ddevil.mirai.command

import me.ddevil.mirai.Mirai
import net.dv8tion.jda.core.events.message.MessageReceivedEvent

abstract class Command(val defaultRequiredLevel: Int, val description: String, vararg val commandName: String) {
    abstract fun execute(mirai: Mirai, args: CommandArgs, event: MessageReceivedEvent)
    val allAliases = commandName.joinToString(", ", "**", "**")
    val primaryName = commandName[0]
}

