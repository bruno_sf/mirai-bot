package me.ddevil.mirai.command

import me.ddevil.mirai.Mirai
import me.ddevil.mirai.util.isBanned
import net.dv8tion.jda.core.entities.User
import net.dv8tion.jda.core.events.message.MessageReceivedEvent

class SetDebugCommand : Command(2, "Seta o canal de debug da Mirai", "setdebug") {
    override fun execute(mirai: Mirai, args: CommandArgs, event: MessageReceivedEvent) {
        val guild = event.guild
        val channel = event.channel
        args.getStringOrMessage(0, "Você deve informar um canal de texto!", channel) {
            val ch = guild.getTextChannelsByName(it, true).first()
            if (ch == null) {
                mirai.sendMessage("Canal $it desconhecido!", channel)
                return
            }
            mirai.getGuildConfig(event.guild).debugChannel = ch
            mirai.sendMessage("Canal de debug setado para ${ch.name}!", channel)
        }
    }
}

class SetMusicCommand : Command(2, "Seta o canal de musica da Mirai", "setmusic") {
    override fun execute(mirai: Mirai, args: CommandArgs, event: MessageReceivedEvent) {
        val guild = event.guild
        val channel = event.channel
        args.getStringOrMessage(0, "Você deve informar um canal de texto!", channel) {
            val ch = guild.getTextChannelsByName(it, true).first()
            if (ch == null) {
                mirai.sendMessage("Canal $it desconhecido!", channel)
                return
            }
            mirai.getGuildConfig(event.guild).musicChannel = ch
            mirai.sendMessage("Canal de musica setado para ${ch.name}!", channel)
        }
    }
}

class MuteCommand : Command(2, "Mutar ou desmutar um usuário", "mute", "m") {
    override fun execute(mirai: Mirai, args: CommandArgs, event: MessageReceivedEvent) {
        val guild = event.guild
        val controller = guild.controller
        val memberName = args[0]
        val author = event.author
        val ch = event.channel
        if (memberName == null) {
            mirai.sendMessage("Mano, vc tem autista? Vc precisa falar o nome de um usuário para mutar", ch)
            return
        }

        val member = guild.getMembersByName(memberName, true).firstOrNull()
        if (member == null) {
            mirai.sendMessage("Usuário $memberName desconhecido", ch)
            return
        }
        val muted = member.voiceState.isGuildMuted
        controller.setMute(member, !muted).queue({
            if (muted) {
                mirai.sendMessage("${member.effectiveName} foi desmutado.", ch)
            } else {
                mirai.sendMessage("${member.effectiveName} foi mutado.", ch)
            }
        }) {
            mirai.sendMessage("Deu ruim: ${it.message}", ch)
        }

    }
}

class BanCommand : Command(2, "Banir ou desbanir um usuário", "ban", "b") {
    override fun execute(mirai: Mirai, args: CommandArgs, event: MessageReceivedEvent) {
        val guild = event.guild
        val controller = guild.controller
        val memberName = args[0]
        val ch = event.channel
        if (memberName == null) {
            mirai.sendMessage("Mano, vc tem autista? Vc precisa falar o nome de um usuário para mutar", ch)
            return
        }

        val member = guild.getMembersByName(memberName, true).firstOrNull()
        if (member == null) {
            mirai.sendMessage("Usuário $memberName desconhecido", ch)
            return
        }
        val banned = guild.controller.isBanned(member)
        if (banned) {
            controller.ban(member, 0).queue({
                mirai.sendMessage("${member.effectiveName} foi banido.", ch)

            }) {
                mirai.sendMessage("Deu ruim: ${it.message}", ch)
            }
        } else {
            controller.unban(member.user).queue({
                mirai.sendMessage("${member.effectiveName} foi desbanido.", ch)

            }) {
                mirai.sendMessage("Deu ruim: ${it.message}", ch)
            }
        }

    }
}

class ListKidWorldCommand : Command(2, "Lista todas as palavras kid", "listKid", "lk") {
    override fun execute(mirai: Mirai, args: CommandArgs, event: MessageReceivedEvent) {
        var msg = "Palavras kids: ${System.lineSeparator()}"
        msg += mirai.kidPreventer.kidWords.joinToString(postfix = System.lineSeparator())
        mirai.sendMessage(msg, event.channel)
    }
}

class RemoveKidWordCommand : Command(2, "Remove uma palavra kid do KidPreventer", "removeKidWord", "rkw") {
    override fun execute(mirai: Mirai, args: CommandArgs, event: MessageReceivedEvent) {
        val word = args.joinFrom(0)
        val kp = mirai.kidPreventer
        val ch = event.channel
        if (!kp.isKidWord(word)) {
            mirai.sendMessage("Esta palavra não está registrada!", ch)
            return
        }
        kp.removeKidWord(word)
        mirai.sendMessage("Palavra '$word' desregistrada!", ch)
    }
}

class AddKidWordCommand : Command(2, "Adiciona uma palavra kid do KidPreventer", "addKidWord", "akw") {
    override fun execute(mirai: Mirai, args: CommandArgs, event: MessageReceivedEvent) {
        val word = args.joinFrom(0)
        val kp = mirai.kidPreventer
        val ch = event.channel
        if (kp.isKidWord(word)) {
            mirai.sendMessage("Esta palavra já está registrada!", ch)
            return
        }
        kp.addKidWord(word)
        mirai.sendMessage("Palavra '$word' registrada!", ch)
    }
}

class UserPermissionCommand : Command(2, "Gerencia o nivel de permissão de usuários", "perm") {
    override fun execute(mirai: Mirai, args: CommandArgs, event: MessageReceivedEvent) {
        val ch = event.channel
        val target: User
        val m = args.getMember(1, event.guild)
        if (m == null) {
            mirai.sendMessage("Usuário desconhecido.", ch)
            return
        }
        target = m.user
        if (args.length() > 1) {
            args.getIntOrMessage(1, "Você deve informar um valor!", "Numéro inválido!", ch) { level ->

                mirai.getGuildConfig(event.guild).permissionManager[target] = level
                mirai.sendMessage("Nivel de permissão de **${target.name}** alterado para **$level**", ch)

            }
        } else {
            val level = mirai.getGuildConfig(event.guild).permissionManager[target]
            mirai.sendMessage("Nivel de permissão de **${target.name}** = **$level**", ch)

        }

    }
}

class CommandPermissionCommand : Command(2, "Altera o nivel de permissão", "perm") {
    override fun execute(mirai: Mirai, args: CommandArgs, event: MessageReceivedEvent) {
        val ch = event.channel
        var target: Command
        args.getStringOrMessage(0, "Você precisa informar um comando", ch) { cmdName ->
            val cmd = mirai.getCommand(cmdName)
            if (cmd == null) {
                mirai.sendMessage("Comando $cmdName desconhecido", ch)
                return
            }
            target = cmd
            if (args.length() > 1) {
                args.getIntOrMessage(1, "Você deve informar um valor!", "Numéro inválido!", ch) { level ->

                    mirai.getGuildConfig(event.guild).permissionManager[target] = level
                    mirai.sendMessage("Nivel de permissão de **${target.primaryName}** alterado para **$level**", ch)

                }
            } else {
                val level = mirai.getGuildConfig(event.guild).permissionManager[target]
                mirai.sendMessage("Nivel de permissão de **${target.primaryName}** = **$level**", ch)

            }

        }
    }

}