package me.ddevil.mirai.command

import me.ddevil.mirai.Mirai
import net.dv8tion.jda.core.entities.Game
import net.dv8tion.jda.core.events.message.MessageReceivedEvent

class HelpCommand : Command(0, "Lista todos os comandos", "help", "ajuda") {
    override fun execute(mirai: Mirai, args: CommandArgs, event: MessageReceivedEvent) {
        val cmds = mirai.commands.filter { mirai.getGuildConfig(event.guild).permissionManager.canUse(event.author, it) }
        var msg = "Comandos conhecidos (${cmds.size}):${System.lineSeparator()}"
        msg += cmds.joinToString { "${it.allAliases} (${it.description})${System.lineSeparator()}" }
        mirai.sendMessage(msg, event.channel)

    }
}

class EchoCommand : Command(0, "Repete uma mensagem", "echo") {
    override fun execute(mirai: Mirai, args: CommandArgs, event: MessageReceivedEvent) {
        val echo = args.joinFrom(0)
        val ch = event.channel
        if (echo.isNullOrEmpty()) {
            mirai.sendMessage("~Oi, eu sou uma pessoa que quer que você repita algo sem eu dizer~${System.lineSeparator()}(－‸ლ)", ch)
            return
        }
        mirai.sendMessage(echo, ch)
    }

}

class GoogleCommand : Command(0, "Dá respostas obvias para perguntas obvias", "google") {
    override fun execute(mirai: Mirai, args: CommandArgs, event: MessageReceivedEvent) {
        var msg = "https://www.lmgtfy.com/?"
        val internetExplainer = args[0] == "-ie"
        val index = if (internetExplainer) {
            1
        } else {
            0
        }
        if (internetExplainer) {
            msg += "iie=1&"

        }
        msg += "q=${args.joinFrom(index).replace(" ", "%20")}"
        mirai.sendMessage(msg, event.channel)
    }
}


class PingCommand : Command(0, "Ping?", "ping") {
    override fun execute(mirai: Mirai, args: CommandArgs, event: MessageReceivedEvent) {
        mirai.sendMessage("Pong! (${mirai.jda.ping}ms)", event.channel)
    }
}

class CommandPrefixCommand : Command(2, "Altera o prefixo usado para commandos", "cmdprefix") {
    override fun execute(mirai: Mirai, args: CommandArgs, event: MessageReceivedEvent) {
        if (args.isEmpty()) {
            mirai.sendMessage("O caractere atual de comandos é '${mirai.commandChar}'", event.channel)
        } else {
            val msg = args.getUnsafe(0)
            val c = msg[0]
            mirai.commandChar = c
            mirai.sendMessage("O caractere de comandos alterado para '$c'", event.channel)
        }
    }
}

class HelloCommand : Command(0, "あいさつ", "Olá!", "olá", "oi", "oi!", "hello", "こんにちは") {
    override fun execute(mirai: Mirai, args: CommandArgs, event: MessageReceivedEvent) {
        if (args.label.equals("こんにちは", false)) {
            mirai.sendMessage("こんにちは ${event.author.name}さん! :D", event.channel)

        } else {
            mirai.sendMessage("Olá ${event.author.name}! :D", event.channel)
        }
    }
}

class GameCommand : Command(3, "Seta o \"jogo\" sendo jogado pela Mirai", "game", "g") {
    override fun execute(mirai: Mirai, args: CommandArgs, event: MessageReceivedEvent) {
        val game = args.joinFrom(0)
        if (game.isNullOrEmpty()) {
            mirai.sendMessage("Você deve informar um jogo!", event.channel)
            return
        }
        val g = Game.of(game)
        mirai.sendMessage("Estou jogando ${g.name} agora!", event.channel)
        mirai.jda.presence.game = g
    }
}