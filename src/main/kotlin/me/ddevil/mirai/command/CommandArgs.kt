package me.ddevil.mirai.command

import me.ddevil.mirai.Mirai
import net.dv8tion.jda.core.entities.Guild
import net.dv8tion.jda.core.entities.Member
import net.dv8tion.jda.core.entities.MessageChannel

data class CommandArgs(val mirai: Mirai, val sender: Member, val label: String, val args: List<String>) {
    /**
     * Gets the argument at the specified index

     * @param index The index to translate
     * *
     * @return The string at the specified index
     */
    operator fun get(index: Int): String? {
        if (length() > index) {
            return args[index]
        }
        return null
    }

    fun getUnsafe(index: Int): String {
        return args[index]
    }

    inline fun getMemberOr(index: Int, orElse: () -> Unit,guild: Guild, action: (Member) -> Unit) {
        if (length() > index) {
            val player = getMember(index, guild)
            if (player != null) {
                action(player)
                return
            }
        }
        orElse()
    }


    fun getMember(index: Int, guild: Guild): Member? {
        if (length() > index) {
            return guild.getMembersByName(this[index], true).firstOrNull()
        }
        return null
    }

    fun getPlayerOrDefault(index: Int,guild: Guild, default: Member): Member {
        if (length() > index) {
            return getMember(index, guild) ?: default
        }
        return default
    }

    @JvmOverloads
    inline fun joinFromAnd(start: Int, end: Int = args.size - 1, action: (String) -> Unit) {
        action(joinFrom(start, end))
    }

    @JvmOverloads
    inline fun joinFrom(start: Int, end: Int = args.size - 1): String {
        var istart = start
        if (length() < start) {
            return ""
        }
        var message = String()
        val containsInitialQuote = getUnsafe(start)[0] == '"'
        if (containsInitialQuote) {
            istart++
        }
        if (length() > istart) {
            for (i in start..args.size - 1) {
                if (i > end) {
                    break
                }
                val string = args[i]
                for (char in string) {
                    if (containsInitialQuote && char == '"') {
                        break
                    }
                    message += char
                }
                message += ' '
            }
        }
        return message
    }

    inline fun getStringOrElse(index: Int, orElse: () -> Unit, action: (String) -> Unit) {
        if (length() > index) {
            action(getUnsafe(index))
        } else {
            orElse()
        }
    }

    inline fun getIntOrElse(index: Int,
                            orElse: () -> Unit,
                            invalidInt: (String) -> Unit,
                            action: (Int) -> Unit) {
        getStringOrElse(index, orElse) {
            try {
                action(it.toInt())
            } catch(e: NumberFormatException) {
                invalidInt(it)
            }
        }
    }


    inline fun getLongOrElse(index: Int,
                             orElse: () -> Unit,
                             invalidLong: (String) -> Unit,
                             action: (Long) -> Unit) {
        getStringOrElse(index, orElse) {
            try {
                action(it.toLong())
            } catch(e: NumberFormatException) {
                invalidLong(it)
            }
        }
    }

    inline fun getFloatOrElse(index: Int,
                              orElse: () -> Unit,
                              invalidFloat: (String) -> Unit,
                              action: (Float) -> Unit) {
        getStringOrElse(index, orElse) {
            try {
                action(it.toFloat())
            } catch(e: NumberFormatException) {
                invalidFloat(it)
            }
        }
    }

    inline fun getDoubleOrElse(index: Int,
                               orElse: () -> Unit,
                               invalidDouble: (String) -> Unit,
                               action: (Double) -> Unit) {
        getStringOrElse(index, orElse) {
            try {
                action(it.toDouble())
            } catch(e: NumberFormatException) {
                invalidDouble(it)
            }
        }
    }

    inline fun getStringOrMessage(index: Int, orElse: String, channel: MessageChannel, action: (String) -> Unit) {
        getStringOrElse(index, {
            mirai.sendMessage(orElse, channel)
        }, action)
    }

    inline fun getIntOrMessage(index: Int, orElse: String, invalidInt: String, channel: MessageChannel, action: (Int) -> Unit) {
        getIntOrElse(index, {
            mirai.sendMessage(orElse, channel)
        }, {
            mirai.sendMessage(invalidInt, channel)
        }, action)
    }

    inline fun getDoubleOrMessage(index: Int, orElse: String, channel: MessageChannel, invalidDouble: String, action: (Double) -> Unit) {
        getDoubleOrElse(index, {
            mirai.sendMessage(orElse, channel)
        }, {
            mirai.sendMessage(invalidDouble, channel)
        }, action)
    }

    inline fun getLongOrMessage(index: Int, orElse: String, channel: MessageChannel, invalidLong: String, action: (Long) -> Unit) {
        getLongOrElse(index, {
            mirai.sendMessage(orElse, channel)
        }, {
            mirai.sendMessage(invalidLong, channel)
        }, action)
    }


    inline fun getFloatOrMessage(index: Int, orElse: String, channel: MessageChannel, invalidFloat: String, action: (Float) -> Unit) {
        getFloatOrElse(index, {
            mirai.sendMessage(orElse, channel)
        }, {
            mirai.sendMessage(invalidFloat, channel)
        }, action)
    }

    /**
     * Returns the length of the command arguments

     * @return int length of args
     */
    fun length() = args.size

    fun isEmpty() = args.isEmpty()
}