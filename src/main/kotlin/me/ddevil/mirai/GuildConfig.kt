package me.ddevil.mirai

import me.ddevil.mirai.music.MusicQueue
import me.ddevil.mirai.permission.PermissionManager
import net.dv8tion.jda.core.entities.Guild
import net.dv8tion.jda.core.entities.TextChannel
import net.dv8tion.jda.core.entities.User

class GuildConfig(mirai: Mirai, guild: Guild) {

    var defaultChannel = guild.publicChannel!!
    var debugChannel: TextChannel
    var musicChannel: TextChannel
    val musicQueue = MusicQueue(mirai, this)
    val permissionManager = PermissionManager(guild, mirai)

    init {
        this.debugChannel = defaultChannel
        this.musicChannel = defaultChannel
    }

}