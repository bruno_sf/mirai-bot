package me.ddevil.mirai.permission

import me.ddevil.mirai.Mirai
import me.ddevil.mirai.command.Command
import net.dv8tion.jda.core.entities.Guild
import net.dv8tion.jda.core.entities.User

class PermissionManager(val guild: Guild, mirai: Mirai) {
    private val userLevelMap: MutableMap<User, Int> = HashMap()
    private val commandLevelMap: MutableMap<Command, Int> = HashMap()

    init {
        mirai.commands.forEach {
            this[it] = it.defaultRequiredLevel
        }
    }


    operator fun get(user: User) = userLevelMap.getOrElse(user) { 0 }

    operator fun set(user: User, level: Int) {
        userLevelMap[user] = level
    }

    operator fun get(command: Command) = commandLevelMap.getOrElse(command) { 0 }
    operator fun set(command: Command, level: Int) {
        commandLevelMap[command] = level
    }

    fun canUse(author: User, command: Command) = this[author] >= this[command]
}