package me.ddevil.mirai.util

import com.sedmelluq.discord.lavaplayer.track.AudioTrackInfo
import net.dv8tion.jda.core.entities.Guild
import net.dv8tion.jda.core.entities.Member
import net.dv8tion.jda.core.entities.TextChannel
import net.dv8tion.jda.core.entities.VoiceChannel
import net.dv8tion.jda.core.managers.GuildController

fun Guild.getTextChannel(member: Member): TextChannel? {
    return textChannels.firstOrNull { it.members.contains(member) }
}

fun <K, V> MutableMap<K, V>.getOrPut(key: K, value: () -> V): V {
    if (containsKey(key)) {
        return this[key]!!
    }
    val v = value()
    this[key] = v
    return v
}
val AudioTrackInfo.toText get() = "$title - $author"
fun GuildController.isBanned(member: Member) = bans.complete().any { member.user == it }
fun Guild.getVoiceChannel(member: Member): VoiceChannel? {
    return voiceChannels.firstOrNull { it.members.contains(member) }
}