package me.ddevil.mirai.storage

interface ConfigManager {
    operator fun <T> get(key: ConfigKey<T>): T
}

interface ConfigKey<T>