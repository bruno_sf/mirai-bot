package me.ddevil.mirai.storage

data class ClanMember(val name: String,
                      val clanPosition: ClanPosition = ClanPosition.DEFAULT) {

}