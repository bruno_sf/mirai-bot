package me.ddevil.mirai.action

interface Action {

    fun execute()

    fun undo()

}