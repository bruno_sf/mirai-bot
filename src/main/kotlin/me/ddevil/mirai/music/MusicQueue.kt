package me.ddevil.mirai.music

import com.sedmelluq.discord.lavaplayer.player.DefaultAudioPlayerManager
import com.sedmelluq.discord.lavaplayer.player.event.AudioEvent
import com.sedmelluq.discord.lavaplayer.player.event.AudioEventListener
import com.sedmelluq.discord.lavaplayer.player.event.TrackEndEvent
import com.sedmelluq.discord.lavaplayer.source.AudioSourceManagers
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist
import com.sedmelluq.discord.lavaplayer.track.AudioTrack
import com.sedmelluq.discord.lavaplayer.track.AudioTrackEndReason
import me.ddevil.mirai.GuildConfig
import me.ddevil.mirai.Mirai
import me.ddevil.mirai.util.toText
import net.dv8tion.jda.core.entities.MessageChannel
import java.util.logging.Logger

class MusicQueue(
        val mirai: Mirai,
        val guild: GuildConfig) : AudioEventListener {

    override fun onEvent(event: AudioEvent) {
        if (event is TrackEndEvent && event.endReason == AudioTrackEndReason.FINISHED) {
            playNext()
        }
    }

    val audioManager = DefaultAudioPlayerManager()
    val player = audioManager.createPlayer()!!
    val wrapper = AudioPlayerSendHandler(player)
    val queue: MutableList<AudioTrack> = ArrayList()

    var playing: Boolean = false

    var currentTrack: AudioTrack? = null
        private set

    fun playNext(channel: MessageChannel = guild.musicChannel): AudioTrack? {
        if (!playing) {
            playing = true
        }
        val next = getNextAndRemove()
        if (next != null) {
            player.playTrack(next)
            this.currentTrack = next
            mirai.sendMessage("Tocando **${next.info.toText}**", channel)
        }
        return next
    }

    init {
        AudioSourceManagers.registerRemoteSources(audioManager)
        player.addListener(this)
    }

    fun addToQueue(music: AudioTrack) {
        Logger.getAnonymousLogger().info("Loaded music ${music.info.title} (${music.info.uri})")
        if (queue.isEmpty() && currentTrack == null) {
            currentTrack = music
        } else {
            queue.add(music)
        }

    }

    fun clear() {
        queue.clear()
    }

    fun getNextAndRemove(): AudioTrack? {
        if (queue.isEmpty()) {
            return null
        }
        return queue.removeAt(0)
    }

    fun addToQueue(music: AudioPlaylist) {
        for (track in music.tracks) {
            addToQueue(track)
        }
    }

    fun stop() {
        if (playing) {
            playing = false
            player.isPaused = true
        }
    }

    fun hasNext() = queue.size > 1

    fun play() {
        if (!playing) {
            playing = true
            val ct = currentTrack
            if (player.playingTrack == null && ct != null) {
                mirai.sendMessage("Tocando **${ct.info.toText}**", guild.musicChannel)
                player.playTrack(currentTrack)
            }
            player.isPaused = false

        }
    }

}